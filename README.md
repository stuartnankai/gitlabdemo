# chatbot

# USAGE:

Go to directory $project/server and run python3 server.py to run python server

Go to directory $project/static and run npm run watch to run and build client code

# GOAL:

The Goal is to build a chatbot which looks like slack and has NLP transformation in built.
Also build bot with latest features of a chatbot like CUI, Graphics, passive mode, Voice Recognition, Alexa/Google etc.
Possible tech stack - Python, React, React Native, Webpack, Flask, Web Socket, Machine Learning/NLP,PyInstaller maybe Electron and zeromq

Phase A :

    1. Build Python POC with flask and socket - DONE
    2. Integrate Python and React - DONE
    3. POC for create a bundle for the app - DONE
    
Phase B :

    1. Improve React components and UI
    2. Build Webapp which will have chat screen to send messages to backend
    3. Investiagte few use cases like AWS cloudwatch
    4. Allow multiple users to chat with each other inside single channel
    5. Add NLP engine
    
Phase C :
     
     # Need to add detailed steps
     1. Create IOS and android app for bot using React native maybe


Phase D :

     # Need to add detailed steps
     1. Create desktop app similar to Slack using electron may be

