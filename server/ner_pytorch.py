import torch
import torch.autograd as autograd  #
import torch.nn as nn  #
import torch.nn.functional as F  #
import torch.optim as optim  #
import pandas as pd
from sklearn.model_selection import train_test_split

# Hyper parameters
EMBEDDING_DIM = 32  #
HIDDEN_DIM = 32  #
EPOCH = 400
LR = 0.1
PATH = './pytorchmodel'

torch.manual_seed(1)

data_set = []
word_to_ix = {}
tag_to_ix = {}

# IOB dataset
word_list = pd.read_csv("trainsmall.txt", sep=' ', header=None)
word_list = word_list[[0, 1]]
word_list.columns = ['word', 'tag']
word_list = word_list.drop(word_list[word_list.word == '-DOCSTART-'].index)

# save tag to index
dfList = list(set(word_list['tag'].tolist()))
for i in dfList:
    tag_to_ix[i] = dfList.index(i)

train_sample_list = ([], [])

for index, row in word_list.iterrows():
    # print (row['word'], row['tag'])
    if row['word'] != ' ':
        train_sample_list[0].append(row['word'])
        train_sample_list[1].append(row['tag'])
    else:
        data_set.append(train_sample_list)
        train_sample_list = ([], [])
    if row['word'] not in word_to_ix:
        word_to_ix[row['word']] = len(word_to_ix)

# Split the data to training and testing part
train_data, test_data = train_test_split(data_set, test_size=0.2)

test_word = [i[0] for i in test_data]
test_tag = [i[1] for i in test_data]


# device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

# print(device)

# print(torch.cuda.is_available())

class LSTMTagger(nn.Module):

    def __init__(self, embedding_dim, hidden_dim, vocab_size, tagset_size):
        super(LSTMTagger, self).__init__()
        self.hidden_dim = hidden_dim

        self.word_embeddings = nn.Embedding(vocab_size, embedding_dim)

        self.lstm = nn.LSTM(embedding_dim, hidden_dim)

        self.hidden2tag = nn.Linear(hidden_dim, tagset_size)
        self.hidden = self.init_hidden()

    def init_hidden(self):
        return (autograd.Variable(torch.zeros(1, 1, self.hidden_dim)),
                autograd.Variable(torch.zeros(1, 1, self.hidden_dim)))

    def forward(self, sentence):
        # print("This is sentence: ", sentence)
        # for i in sentence:
        #     print(list(word_to_ix.keys())[list(word_to_ix.values()).index(i.item())])
        embeds = self.word_embeddings(sentence)
        lstm_out, self.hidden = self.lstm(
            embeds.view(len(sentence), 1, -1), self.hidden)
        tag_space = self.hidden2tag(lstm_out.view(len(sentence), -1))
        tag_scores = F.log_softmax(tag_space)
        return tag_scores


def prepare_sequence(seq, to_ix):
    idxs = [to_ix[w] for w in seq]
    tensor = torch.LongTensor(idxs)
    return autograd.Variable(tensor)


def getPredictionTag(tag_scores):
    tag_list = []
    for i in tag_scores:
        max_value = torch.max(i)
        index_value = (i == max_value).nonzero()
        tag = (list(tag_to_ix.keys())[list(tag_to_ix.values()).index(index_value)])
        tag_list.append(tag)
    return tag_list


def matchPercentage(pre_tag, real_tag):
    count = 0
    for index, value in enumerate(pre_tag):
        if real_tag[index] == value:
            count += 1
    return count / len(pre_tag)


model = LSTMTagger(EMBEDDING_DIM, HIDDEN_DIM, len(word_to_ix), len(tag_to_ix))
loss_function = nn.NLLLoss()
optimizer = optim.SGD(model.parameters(), lr=LR)

# Save and load model
# torch.save(model.state_dict(), PATH)
# the_model = LSTMTagger(EMBEDDING_DIM, HIDDEN_DIM, len(word_to_ix), len(tag_to_ix))
# the_model.load_state_dict(torch.load(PATH))

for epoch in range(EPOCH):
    for sentence, tags in train_data:
        #
        model.zero_grad()
        #
        model.hidden = model.init_hidden()
        #
        sentence_in = prepare_sequence(sentence, word_to_ix)
        targets = prepare_sequence(tags, tag_to_ix)
        #
        tag_scores = model(sentence_in)
        #
        loss = loss_function(tag_scores, targets)
        loss.backward()
        optimizer.step()
        #
        if epoch % 50 == 0:
            test_output = model(prepare_sequence(test_word[0], word_to_ix))
            pre_tag = getPredictionTag(test_output)

            print('Epoch: ', epoch, '| train loss: %.4f' % loss.data.numpy(),
                  '| test accuracy: %.2f' % matchPercentage(pre_tag, test_tag[0]))

test_sen = ['we', 'must', 'answer', 'Israel']
test_sen_tag = ['PRP', 'MD', 'VB', 'NNP']
inputs = prepare_sequence(test_sen, word_to_ix)
tag_scores = model(inputs)
pre_tag = getPredictionTag(tag_scores)
# print("This is real sentence ", data_set[0][0])
# print("This is real tag ", data_set[0][1])
print("This is prediction tag: ", pre_tag)
print("This is match percentage: ", matchPercentage(pre_tag, test_sen_tag))
