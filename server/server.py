import random,os,sys,json
from flask import Flask, render_template
from flask_socketio import SocketIO, send
from engineio import async_threading
import nltk
import spacy
from spacy import displacy



output_dir = "en_core_web_sm"
nlp = spacy.load(output_dir)

base_dir = '.'
if hasattr(sys, '_MEIPASS'):
    base_dir = os.path.join(sys._MEIPASS)
else:
    base_dir = base_dir + './'

app = Flask(__name__, 
            static_folder=os.path.join(base_dir, 'static/dist'),
             template_folder=os.path.join(base_dir, 'static'))
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app)
@app.route('/')
def index():
    return render_template('index.html')


@app.route('/hello') 
def hello():
    return get_superheroes()

@app.route('/chatapp')
def chat_app():
    return render_template('chatroom.html')


def get_superheroes():
    superheroes_list = ['Hulk', 'Iron Man', 'Doctor Strange', 'Thor', 'Black Widow', 'Thanos']
    return random.choice(superheroes_list)

@socketio.on('join')
def handle_my_join(jso):
    print('received join: ' + str(jso))

@socketio.on('leave')
def handle_my_leave(jso):
    print('received leave: ' + str(jso))

@socketio.on('register')
def handle_my_register(jso):
    print('received register: ' + str(jso))

@socketio.on('availableUsers')
def handle_my_availableUsers(jso):
    print('received availableUsers: ' + str(jso))
    socketio.emit('userresponse', jso)

@socketio.on('chatrooms')
def handle_my_chatrooms(ch):
    print('received chatrooms: ' )
    ch = [{'name': 'Terminus','image': '../images/terminus.jpg'}]
    return None,ch

@socketio.on('message')
def handle_my_message(jso):
    print('received message: ' + str(jso))

@socketio.on('disconnect')
def handle_my_disconnect(jso):
    print('received disconnect: ' + str(jso))


@socketio.on('error')
def handle_my_error(jso):
    print('received error: ' + str(jso))


# capture the messages from all users inside single channel
@socketio.on('chatImage')
def handel_hunman_message(msg):
    str1 = ''.join(str(e) for e in msg.values())
    print("This is human message: ", str1)
    doc = nlp(str1)
    #
    # labels = [x.label_ for x in doc.ents]
    # print(Counter(labels))
    #
    # items = [x.text for x in doc.ents]
    # print(Counter(items).most_common(3))
    #
    # html = displacy.render([doc], style='dep', page=True)

    svg = displacy.render(doc, style='dep')
    socketio.emit('showImage', svg)


  

if __name__ == '__main__':
    socketio.run(app,debug=True)
