import React from "react";
import { Button, Grid, Row, Col } from "react-bootstrap";


const hstyle = {
    color:'#FFFFFF'
}
export default class Hello extends React.Component {
    constructor(props) {
        super(props);
        this.state = {greeting: this.props.name +' Hulk '};

        // This binding is necessary to make `this` work in the callback
        this.getSuperHero = this.getSuperHero.bind(this);
    }

    personaliseGreeting(greeting) {
        this.setState({greeting:   this.props.name + ' ' +greeting + '!'});
    }

    getSuperHero() {
        var me = this;
        fetch(window.location.href + 'hello',{

        }).then(function(response){
            return response.text();
            
        }).then(function(text){
            console.log(text);
            me.personaliseGreeting(text);
        }).catch(function(err){
            console.log(err);
        })
        
    }

    render () {
        return (
            <Grid>
                <Row>
                <Col md={12} >
                    <h1 style={hstyle}>{this.state.greeting}</h1>
                    <hr/>
                </Col>
                </Row>
                <Row>
                <Col md={7} mdOffset={5}>
                    <Button bsSize="large" bsStyle="danger" onClick={this.getSuperHero}>
                    Invite SuperHeroes!
                    </Button>
                </Col>
                </Row>
            </Grid>
        );
    }
}
